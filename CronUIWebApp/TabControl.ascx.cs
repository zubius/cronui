﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CronUIWebApp
{
    public partial class TabControl : System.Web.UI.UserControl
    {
        private List<TextBox> allTBs = new List<TextBox>();
        private List<TextBox> minuteTB = new List<TextBox>();
        private List<TextBox> hourTB = new List<TextBox>();
        private List<TextBox> dayTB = new List<TextBox>();
        private List<TextBox> monthTB = new List<TextBox>();
        private const string ASTERISK = "*";

        protected void Page_Load(object sender, EventArgs e)
        {
            InitAllTB();
            InitMinuteTB();
            InitHourTB();
            InitDayTB();
            InitMonthTB();

            if (!IsPostBack)
            {
                DaySelect_ddl.SelectedIndex = -1;
                atDoW_ddl.SelectedIndex = -1;
                fromDoW_ddl.SelectedIndex = -1;
                toDoW_ddl.SelectedIndex = -1;
                TabContainer.ActiveTabIndex = 0;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        public void LoadCron()
        {
            string minute = CronObject.Minutes.Value;
            string hour = CronObject.Hours.Value;
            string day = CronObject.Days.Value;
            string month = CronObject.Months.Value;
            string dow = CronObject.DaysOfWeek.Value;

            int var = (int)CronObject.Minutes.Variation + (int)CronObject.Hours.Variation + 
                (int)CronObject.Days.Variation + (int)CronObject.Months.Variation + 
                (int)CronObject.DaysOfWeek.Variation;
            if (var == 0)
            {
                if (day == ASTERISK && month == ASTERISK &&
                    minute != ASTERISK && hour != ASTERISK && dow != ASTERISK)
                {
                    WeeklyEnable();

                    Daily_rb.Checked = false;
                    Monthly_rb.Checked = false;
                    Custom_rb.Checked = false;
                    Weekly_rb.Checked = true;

                    DaySelect_ddl.SelectedIndex = int.Parse(dow);
                    SetTimeAtWeekly_tb.Text = hour + ":" + minute;
                }
                else if (day == ASTERISK && month == ASTERISK &&
                    minute != ASTERISK && hour == ASTERISK && dow == ASTERISK)
                {
                    DailyEnable();

                    Daily_rb.Checked = true;
                    int i = minute.IndexOf("/");
                    eachNMin_tb.Text = minute.Substring(i + 1);
                }
                else if (day != ASTERISK && month == ASTERISK &&
                    minute != "1" && hour != "0" && dow == ASTERISK)
                {
                    MonthlyEnable();

                    Monthly_rb.Checked = true;
                    SetMontly_tb.Text = day;
                }
            }
            else
            {
                CustomEnable();
                MinutesLoad(CronObject.Minutes.Variation, minute);
                HourLoad(CronObject.Hours.Variation, hour);
                DayLoad(CronObject.Days.Variation, day);
                MonthLoad(CronObject.Months.Variation, month);
                DayOfWeekLoad(CronObject.DaysOfWeek.Variation, dow);
            }
        }

        #region Weekly
        protected void Weekly_rb_CheckedChanged(object sender, EventArgs e)
        {
            WeeklyEnable();

            CronObject.Reset();

            CronObject.Days.Value = ASTERISK;
            CronObject.Months.Value = ASTERISK;
            CronObject.DaysOfWeek.Value = "0";

            Panel1_CollapsiblePanelExtender.Collapsed = true;
            Panel1_CollapsiblePanelExtender.ClientState = "true";
        }

        private void WeeklyEnable()
        {
            ClearTB(allTBs);

            DaySelect_ddl.Enabled = true;
            SetTimeAtWeekly_tb.Enabled = true;

            eachNMin_tb.Enabled = false;
            SetMontly_tb.Enabled = false;
        }

        protected void DaySelect_ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            CronObject.DaysOfWeek.Value = DaySelect_ddl.SelectedIndex.ToString();
        }

        protected void SetTimeAtWeekly_tb_TextChanged(object sender, EventArgs e)
        {
            string time = SetTimeAtWeekly_tb.Text;
            int i = time.IndexOf(":");
            if (i > 0)
            {
                CronObject.Hours.Value = time.Substring(0, i);
                CronObject.Minutes.Value = time.Substring(i + 1);
            }
        }
        #endregion
        #region Daily
        protected void Daily_rb_CheckedChanged(object sender, EventArgs e)
        {
            DailyEnable();

            CronObject.Reset();

            CronObject.Hours.Value = ASTERISK;
            CronObject.Days.Value = ASTERISK;
            CronObject.Months.Value = ASTERISK;
            CronObject.DaysOfWeek.Value = ASTERISK;

            Panel1_CollapsiblePanelExtender.Collapsed = true;
            Panel1_CollapsiblePanelExtender.ClientState = "true";
        }

        private void DailyEnable()
        {
            ClearTB(allTBs);

            eachNMin_tb.Enabled = true;

            DaySelect_ddl.Enabled = false;
            DaySelect_ddl.SelectedIndex = -1;
            SetTimeAtWeekly_tb.Enabled = false;
            SetMontly_tb.Enabled = false;
        }

        protected void eachNMin_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Minutes.Value = "*/" + eachNMin_tb.Text;
        }
        #endregion
        #region Monthly
        protected void Monthly_rb_CheckedChanged(object sender, EventArgs e)
        {
            MonthlyEnable();

            CronObject.Reset();

            CronObject.Minutes.Value = "1";
            CronObject.Hours.Value = "0";
            CronObject.Months.Value = ASTERISK;
            CronObject.DaysOfWeek.Value = ASTERISK;

            Panel1_CollapsiblePanelExtender.Collapsed = true;
            Panel1_CollapsiblePanelExtender.ClientState = "true";
        }

        private void MonthlyEnable()
        {
            ClearTB(allTBs);

            SetMontly_tb.Enabled = true;

            SetTimeAtWeekly_tb.Enabled = false;
            DaySelect_ddl.Enabled = false;
            DaySelect_ddl.SelectedIndex = -1;
            eachNMin_tb.Enabled = false;
        }

        protected void SetMontly_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = "*/" + SetMontly_tb.Text;
        }
        #endregion
        #region Custom
        protected void Custom_rb_CheckedChanged(object sender, EventArgs e)
        {
            CustomEnable();
            CronObject.Reset();
        }

        private void CustomEnable()
        {
            ClearTB(allTBs);

            SetMontly_tb.Enabled = false;
            SetTimeAtWeekly_tb.Enabled = false;
            DaySelect_ddl.Enabled = false;
            DaySelect_ddl.SelectedIndex = -1;
            eachNMin_tb.Enabled = false;

            Custom_rb.Checked = true;

            Panel1_CollapsiblePanelExtender.Collapsed = false;
            Panel1_CollapsiblePanelExtender.ClientState = "false";
        }

        #region Minute
        protected void atMin_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(minuteTB);

            minute_tb.Enabled = true;

            everyNMin_tb.Enabled = false;
            fromMin_tb.Enabled = false;
            toMin_tb.Enabled = false;
        }
        protected void everyMin_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(minuteTB);

            minute_tb.Enabled = false;
            everyNMin_tb.Enabled = false;
            fromMin_tb.Enabled = false;
            toMin_tb.Enabled = false;
            CronObject.Minutes.Value = ASTERISK;
        }
        protected void everyNMin_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(minuteTB);

            everyNMin_tb.Enabled = true;

            minute_tb.Enabled = false;
            fromMin_tb.Enabled = false;
            toMin_tb.Enabled = false;
        }
        protected void rangeMin_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(minuteTB);

            fromMin_tb.Enabled = true;
            toMin_tb.Enabled = true;

            everyNMin_tb.Enabled = false;
            minute_tb.Enabled = false;
        }
        protected void minute_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Minutes.Value = minute_tb.Text;
        }
        protected void fromMin_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Minutes.Value = fromMin_tb.Text + "-";
        }
        protected void toMin_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Minutes.Value = CronObject.Minutes.Value + toMin_tb.Text;
        }
        protected void everyNMin_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Minutes.Value = "/*" + everyNMin_tb.Text;
        }
        private void MinutesLoad(TokenVar var, string min)
        {
            switch (var)
            {
                case TokenVar.Solo:
                    if (min == "*")
                    {
                        everyMin_rb.Checked = true;
                        everyMin_rb_CheckedChanged(this, EventArgs.Empty);
                        break;
                    }
                    atMin_rb.Checked = true;
                    minute_tb.Text = min;

                    atMin_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Range:
                    rangeMin_rb.Checked = true;
                    fromMin_tb.Text = min.Split(new char[] { '-' })[0];
                    toMin_tb.Text = min.Split(new char[] { '-' })[1];

                    rangeMin_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.List:
                    atMin_rb.Checked = true;
                    minute_tb.Text = min;

                    atMin_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Repeat:
                    everyNMin_rb.Checked = true;
                    everyNMin_tb.Text = min.Substring(min.IndexOf("/") + 1);

                    everyNMin_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                default:
                    break;
            }
        }
        #endregion
        #region Hour
        protected void atHr_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(hourTB);

            hour_tb.Enabled = true;

            everyNHr_tb.Enabled = false;
            fromHr_tb.Enabled = false;
            toHr_tb.Enabled = false;
        }
        protected void everyHr_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(hourTB);

            hour_tb.Enabled = false;
            everyNHr_tb.Enabled = false;
            fromHr_tb.Enabled = false;
            toHr_tb.Enabled = false;

            CronObject.Hours.Value = ASTERISK;
        }
        protected void everyNHr_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(hourTB);

            everyNHr_tb.Enabled = true;

            hour_tb.Enabled = false;
            fromHr_tb.Enabled = false;
            toHr_tb.Enabled = false;
        }
        protected void rangeHr_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(hourTB);

            fromHr_tb.Enabled = true;
            toHr_tb.Enabled = true;

            hour_tb.Enabled = false;
            everyNHr_tb.Enabled = false;
        }
        protected void hour_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Hours.Value = hour_tb.Text;
        }
        protected void fromHr_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Hours.Value = fromHr_tb.Text + "-";
        }
        protected void toHr_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Hours.Value = CronObject.Hours.Value + toHr_tb.Text;
        }
        protected void everyNHr_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Hours.Value = "*/" + everyNHr_tb.Text;
        }
        private void HourLoad(TokenVar var, string hr)
        {
            switch (var)
            {
                case TokenVar.Solo:
                    if (hr == "*")
                    {
                        everyHr_rb.Checked = true;
                        everyHr_rb_CheckedChanged(this, EventArgs.Empty);
                        break;
                    }
                    atHr_rb.Checked = true;
                    hour_tb.Text = hr;

                    atHr_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Range:
                    rangeHr_rb.Checked = true;
                    fromHr_tb.Text = hr.Split(new char[] { '-' })[0];
                    toHr_tb.Text = hr.Split(new char[] { '-' })[1];

                    rangeHr_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.List:
                    atHr_rb.Checked = true;
                    hour_tb.Text = hr;

                    atHr_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Repeat:
                    everyNHr_rb.Checked = true;
                    everyNHr_tb.Text = hr.Substring(hr.IndexOf("/") + 1);

                    everyNHr_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                default:
                    break;
            }
        }
        #endregion
        #region Day
        protected void atDay_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(dayTB);

            day_tb.Enabled = true;

            fromDay_tb.Enabled = false;
            toDay_tb.Enabled = false;
            everyNDay_tb.Enabled = false;
        }
        protected void everyDay_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(dayTB);

            day_tb.Enabled = false;
            fromDay_tb.Enabled = false;
            toDay_tb.Enabled = false;
            everyNDay_tb.Enabled = false;

            CronObject.Days.Value = ASTERISK;
        }
        protected void rangeDay_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(dayTB);

            fromDay_tb.Enabled = true;
            toDay_tb.Enabled = true;

            day_tb.Enabled = false;
            everyNDay_tb.Enabled = false;
        }
        protected void everyNDay_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(dayTB);

            everyNDay_tb.Enabled = true;

            day_tb.Enabled = false;
            fromDay_tb.Enabled = false;
            toDay_tb.Enabled = false;
        }
        protected void day_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = day_tb.Text;
        }
        protected void fromDay_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = fromDay_tb.Text + "-";
        }
        protected void toDay_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = CronObject.Days.Value + toDay_tb.Text;
        }
        protected void everyNDay_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = "*/" + everyNDay_tb.Text;
        }
        private void DayLoad(TokenVar var, string day)
        {
            switch (var)
            {
                case TokenVar.Solo:
                    if (day == "*" || day == "?")
                    {
                        everyDay_rb.Checked = true;
                        everyDay_rb_CheckedChanged(this, EventArgs.Empty);
                        break;
                    }
                    atDay_rb.Checked = true;
                    day_tb.Text = day;

                    atDay_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Range:
                    rangeDay_rb.Checked = true;
                    fromDay_tb.Text = day.Split(new char[] { '-' })[0];
                    toDay_tb.Text = day.Split(new char[] { '-' })[1];

                    rangeDay_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.List:
                    atDay_rb.Checked = true;
                    day_tb.Text = day;

                    atDay_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Repeat:
                    everyNDay_rb.Checked = true;
                    everyNDay_tb.Text = day.Substring(day.IndexOf("/") + 1);

                    everyNDay_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                default:
                    break;
            }
        }
        #endregion
        #region Month
        protected void atMon_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(monthTB);

            month_tb.Enabled = true;

            fromMon_tb.Enabled = false;
            toMon_tb.Enabled = false;
            everyNMonth_tb.Enabled = false;
        }
        protected void everyMon_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(monthTB);

            month_tb.Enabled = false;
            fromMon_tb.Enabled = false;
            toMon_tb.Enabled = false;
            everyNMonth_tb.Enabled = false;

            CronObject.Months.Value = ASTERISK;
        }
        protected void rangeMon_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(monthTB);

            fromMon_tb.Enabled = true;
            toMon_tb.Enabled = true;

            month_tb.Enabled = false;
            everyNMonth_tb.Enabled = false;
        }
        protected void everyNMon_rb_CheckedChanged(object sender, EventArgs e)
        {
            ClearTB(monthTB);

            everyNMonth_tb.Enabled = true;

            fromMon_tb.Enabled = false;
            toMon_tb.Enabled = false;
            month_tb.Enabled = false;
        }
        protected void month_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Months.Value = month_tb.Text;
        }
        protected void fromMon_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = fromMon_tb.Text + "-";
        }
        protected void toMon_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = CronObject.Days.Value + toMon_tb.Text;
        }
        protected void everyNMonth_tb_TextChanged(object sender, EventArgs e)
        {
            CronObject.Days.Value = "*/" + everyNMonth_tb.Text;
        }
        private void MonthLoad(TokenVar var, string mon)
        {
            switch (var)
            {
                case TokenVar.Solo:
                    if (mon == "*")
                    {
                        everyMon_rb.Checked = true;
                        everyMon_rb_CheckedChanged(this, EventArgs.Empty);
                        break;
                    }
                    atMon_rb.Checked = true;
                    month_tb.Text = mon;

                    atMon_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Range:
                    rangeMon_rb.Checked = true;
                    fromMon_tb.Text = mon.Split(new char[] { '-' })[0];
                    toMon_tb.Text = mon.Split(new char[] { '-' })[1];

                    rangeMon_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.List:
                    atMon_rb.Checked = true;
                    month_tb.Text = mon;

                    atMon_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Repeat:
                    everyNMon_rb.Checked = true;
                    everyNMonth_tb.Text = mon.Substring(mon.IndexOf("/") + 1);

                    everyNMon_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                default:
                    break;
            }
        }
        #endregion
        #region Day of Week
        protected void atDoW_rb_CheckedChanged(object sender, EventArgs e)
        {
            atDoW_ddl.Enabled = true;

            fromDoW_ddl.Enabled = false;
            toDoW_ddl.Enabled = false;
            fromDoW_ddl.SelectedIndex = -1;
            toDoW_ddl.SelectedIndex = -1;
        }
        protected void everyDoW_rb_CheckedChanged(object sender, EventArgs e)
        {
            atDoW_ddl.Enabled = false;
            fromDoW_ddl.Enabled = false;
            toDoW_ddl.Enabled = false;
            fromDoW_ddl.SelectedIndex = -1;
            toDoW_ddl.SelectedIndex = -1;
            atDoW_ddl.SelectedIndex = -1;

            CronObject.DaysOfWeek.Value = ASTERISK;
        }
        protected void rangeDoW_rb_CheckedChanged1(object sender, EventArgs e)
        {
            fromDoW_ddl.Enabled = true;
            toDoW_ddl.Enabled = true;

            atDoW_ddl.Enabled = false;
            atDoW_ddl.SelectedIndex = -1;
        }
        protected void workdays_rb_CheckedChanged(object sender, EventArgs e)
        {
            atDoW_ddl.Enabled = false;
            fromDoW_ddl.Enabled = false;
            toDoW_ddl.Enabled = false; fromDoW_ddl.SelectedIndex = -1;
            toDoW_ddl.SelectedIndex = -1;
            atDoW_ddl.SelectedIndex = -1;

            CronObject.DaysOfWeek.Value = "MON-FRI";
        }
        protected void atDoW_ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            CronObject.DaysOfWeek.Value = atDoW_ddl.SelectedIndex.ToString();
        }
        protected void fromDoW_ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            CronObject.DaysOfWeek.Value = fromDoW_ddl.SelectedIndex.ToString() + "-";
        }
        protected void toDoW_ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            CronObject.DaysOfWeek.Value = CronObject.DaysOfWeek.Value + toDoW_ddl.SelectedIndex.ToString();
        }
        private void DayOfWeekLoad(TokenVar var, string dow)
        {
            switch (var)
            {
                case TokenVar.Solo:
                    if (dow == "*")
                    {
                        everyDoW_rb.Checked = true;
                        everyDoW_rb_CheckedChanged(this, EventArgs.Empty);
                        break;
                    }
                    atDoW_rb.Checked = true;
                    atDoW_ddl.SelectedIndex = int.Parse(dow);

                    atDoW_rb_CheckedChanged(this, EventArgs.Empty);
                    break;
                case TokenVar.Range:
                    if (dow.Equals("MON-FRI", StringComparison.InvariantCultureIgnoreCase) || dow.Equals("1-5"))
                    {
                        workdays_rb.Checked = true;
                        workdays_rb_CheckedChanged(this, EventArgs.Empty);
                        break;
                    }
                    rangeDoW_rb.Checked = true;
                    fromDoW_ddl.SelectedIndex = int.Parse(dow.Split(new char[] { '-' })[0]);
                    toDoW_ddl.SelectedIndex = int.Parse(dow.Split(new char[] { '-' })[1]);

                    rangeDoW_rb_CheckedChanged1(this, EventArgs.Empty);
                    break;
                default:
                    break;
            }
        }
        #endregion
        #endregion   

        #region TextBox manipulation
        private void InitAllTB()
        {
            allTBs.Add(day_tb);
            allTBs.Add(eachNMin_tb);
            allTBs.Add(everyNDay_tb);
            allTBs.Add(everyNHr_tb);
            allTBs.Add(everyNMin_tb);
            allTBs.Add(everyNMonth_tb);
            allTBs.Add(fromDay_tb);
            allTBs.Add(fromHr_tb);
            allTBs.Add(fromMin_tb);
            allTBs.Add(fromMon_tb);
            allTBs.Add(hour_tb);
            allTBs.Add(minute_tb);
            allTBs.Add(month_tb);
            allTBs.Add(SetMontly_tb);
            allTBs.Add(SetTimeAtWeekly_tb);
            allTBs.Add(toDay_tb);
            allTBs.Add(toHr_tb);
            allTBs.Add(toMin_tb);
            allTBs.Add(toMon_tb);
        }

        private void InitMinuteTB()
        {
            minuteTB.Add(everyNMin_tb);
            minuteTB.Add(fromMin_tb);
            minuteTB.Add(minute_tb);
            minuteTB.Add(toMin_tb);
        }

        private void InitHourTB()
        {
            hourTB.Add(everyNHr_tb);
            hourTB.Add(fromHr_tb);
            hourTB.Add(hour_tb);
            hourTB.Add(toHr_tb);
        }

        private void InitDayTB()
        {
            dayTB.Add(day_tb);
            dayTB.Add(everyNDay_tb);
            dayTB.Add(fromDay_tb);
            dayTB.Add(toDay_tb);
        }

        private void InitMonthTB()
        {
            monthTB.Add(everyNMonth_tb);
            monthTB.Add(fromMon_tb);
            monthTB.Add(month_tb);
            monthTB.Add(toMon_tb);
        }

        private void ClearTB(List<TextBox> textBoxes)
        {
            foreach (TextBox tb in textBoxes)
            {
                tb.Text = String.Empty;
            }
        }
        #endregion
    }
}