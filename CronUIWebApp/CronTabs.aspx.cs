﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CronUIWebApp
{
    public partial class CronTabs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["CronString"] != null)
            {
                CronString_tb.Text = ViewState["CronString"].ToString();
            }
            InvArg_lbl.Text = "";
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!(String.IsNullOrEmpty(CronString_tb.Text) && String.IsNullOrWhiteSpace(CronString_tb.Text)))
            {
                ViewState["CronString"] = CronString_tb.Text;
            }
        }

        protected void Load_btn_Click(object sender, EventArgs e)
        {
            if (!(String.IsNullOrEmpty(CronString_tb.Text) && String.IsNullOrWhiteSpace(CronString_tb.Text)))
            {
                try
                {
                    CronFormat.ParseCronString(CronString_tb.Text);
                    TabControl1.LoadCron();
                }
                catch (ArgumentException ae)
                {
                    InvArg_lbl.Text = "Invalid formatted string";
                }
            }
        }

        protected void Save_btn_Click(object sender, EventArgs e)
        {
            CronString_tb.Text = CronFormat.FormCronString();
        }
    }
}