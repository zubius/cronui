﻿/// <summary>
/// Types if cron token
/// </summary>
public enum CronType
{
    Minute = 0,
    Hour,
    Day,
    Month,
    DayOfWeek
}
