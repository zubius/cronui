﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>
public static class CronFormat
{
    private static readonly char[] separ = {' '};

    /// <summary>
    /// Generate cron string from cron field objects
    /// </summary>
    /// <returns></returns>
    public static string FormCronString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(CronObject.Minutes.Value); sb.Append(" ");
        sb.Append(CronObject.Hours.Value); sb.Append(" ");
        sb.Append(CronObject.Days.Value); sb.Append(" ");
        sb.Append(CronObject.Months.Value); sb.Append(" ");
        sb.Append(CronObject.DaysOfWeek.Value);

        return sb.ToString();
    }

    /// <summary>
    /// Parse input string to 5 objects for each field
    /// </summary>
    /// <param name="s">Cron string from input</param>
    public static void ParseCronString(string s)
    {
        string[] parts = s.Split(separ, StringSplitOptions.RemoveEmptyEntries);

        if (parts.Length != 5)
        {
            throw new ArgumentException(s + " is not valin cron string. Must contans 5 elements.");
        }

        CronObject.SetCron(parts);
    }
}