﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Complex cron object, consists from 5 objects-tokens
/// </summary>
public class CronObject
{
    public static CronClass Minutes;
    public static CronClass Hours;
    public static CronClass Days;
    public static CronClass Months;
    public static CronClass DaysOfWeek;

    private const string ASTERISK = "*";

    static CronObject()
    {
        Minutes = new CronClass(CronType.Minute, ASTERISK);
        Hours = new CronClass(CronType.Hour, ASTERISK);
        Days = new CronClass(CronType.Day, ASTERISK);
        Months = new CronClass(CronType.Month, ASTERISK);
        DaysOfWeek = new CronClass(CronType.DayOfWeek, ASTERISK);
    }

    public static void Reset()
    {
        Minutes = new CronClass(CronType.Minute, ASTERISK);
        Hours = new CronClass(CronType.Hour, ASTERISK);
        Days = new CronClass(CronType.Day, ASTERISK);
        Months = new CronClass(CronType.Month, ASTERISK);
        DaysOfWeek = new CronClass(CronType.DayOfWeek, ASTERISK);
    }

    /// <exception cref="ArgumentOutOfRangeException">Entered not valid Value</exception>
	public static void SetCron(string[] tokens)
	{
        Minutes = new CronClass(CronType.Minute, tokens[0]);
        Hours = new CronClass(CronType.Hour, tokens[1]);
        Days = new CronClass(CronType.Day, tokens[2]);
        Months = new CronClass(CronType.Month, tokens[3]);
        DaysOfWeek = new CronClass(CronType.DayOfWeek, tokens[4]);

        if (!(Minutes.IsValid && Hours.IsValid &&
            Days.IsValid && Months.IsValid && DaysOfWeek.IsValid))
        {
            throw new ArgumentOutOfRangeException();
        }
	}
}