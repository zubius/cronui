﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

/// <summary>
/// Represents separate token of cron string
/// </summary>
public class CronClass
{
    private static readonly string[][] names = new string[2][];
    private string token;
    private int minValue;
    private int maxValue;
    private string value;
    List<int> intValue = new List<int>();
    private bool isValid;
    CronType type;
    TokenVar var;
    

	public CronClass(CronType type, string token)
	{
        this.type = type;
        this.token = token;

        names[0] = new string[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
        names[1] = new string[] { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };

        switch (type)
        {
            case CronType.Minute:
                minValue = 0; maxValue = 59;
                break;
            case CronType.Hour:
                minValue = 0; maxValue = 23;
                break;
            case CronType.Day:
                minValue = 1; maxValue = 31;
                break;
            case CronType.Month:
                minValue = 1; maxValue = 12;
                break;
            case CronType.DayOfWeek:
                minValue = 0; maxValue = 6;
                break;
        }

        isValid = Validate(token); //TODO: develope reaction for bad inputs;

        if (isValid)
        {
            if (intValue.Count > 0 && intValue.Count == 2)
            {
                value = intValue[0] + "-" + intValue[1];
            }
            else if (intValue.Count > 0 && intValue.Count == 1)
            {
                value = intValue[0].ToString();
            }
            else
            {
                value = token;
            }
        }
	}

    public string Value
    {
        get { return this.value; }
        set { this.value = value; }
    }

    public CronType Type
    {
        get { return this.type; }
    }

    public TokenVar Variation
    {
        get { return this.var; }
    }

    public bool IsValid
    {
        get { return this.isValid; }
    }

    private bool Validate(string token) //first check for complex token
    {
        if (token.Length == 0)
        {
            throw new ArgumentException("Empty string");
        }

        if (token.IndexOf(",") > 0)
        {
            IEnumerator<string> ie = ((IEnumerable<string>)token.Split(new[] { ',' })).GetEnumerator();
            bool value;
            List<string> results = new List<string>();
            while (ie.MoveNext())
            {
                value = (Validate(ie.Current));
                if (!value)
                {
                    return false;
                }

                var = TokenVar.List;
                return true;
            }
        }

        int slashInd = token.IndexOf("/");
        if (slashInd > 0)
        {
            try
            {
                value = token.Substring(slashInd + 1);
            }
            catch (Exception ex)
            {
                return false;
            }

            token = token.Substring(0, slashInd);
            var = TokenVar.Repeat;
        }

        if (token.Length == 1 && token[0] == '*' || token[0] == '?')
        {
            if (var != TokenVar.Repeat)
            {
                var = TokenVar.Solo;
            }
            return true;
        }

        int rangeInd = token.IndexOf("-");
        if (rangeInd > 0)
        {
            bool start = ValidatePart(token.Substring(0, rangeInd));
            bool end = ValidatePart(token.Substring(rangeInd + 1));

            if (!(start && end))
            {
                return false;
            }
            else
            {
                var = TokenVar.Range;
                return true;
            }
        }

        var = TokenVar.Solo;
        return ValidatePart(token); //validating simple token
    }

    private bool ValidatePart(string part)
    {
        char first = part[0];

        if (first >= '0' && first <= '9')
        {
            try
            {
                int val = int.Parse(part);

                if (val >= minValue || val <= maxValue)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        if (type == CronType.Month || type == CronType.DayOfWeek)
        {
            for (int i = 0; i < names[(int)type - 3].Length; i++)
            {
                if (CultureInfo.InvariantCulture.CompareInfo.IsPrefix(         //magic number 3 is
                    names[(int)type - 3][i], part, CompareOptions.IgnoreCase)) //CronType.Month - 3 = 0, CronType.DayOfWeek - 3 = 1 for names index
                {
                    intValue.Add(i);
                    return true;
                }
            }
        }
        return false;
    }
}