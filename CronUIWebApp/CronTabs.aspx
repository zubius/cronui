﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CronTabs.aspx.cs" Inherits="CronUIWebApp.CronTabs" %>

<%@ Register src="TabControl.ascx" tagname="TabControl" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .saveButtonStyle {
            margin-left: 150px;
        }
        .loadButtonStyle {
            margin-left: 41px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <uc1:TabControl ID="TabControl1" runat="server" />
    
    </div>
<div style="margin-left: 33%; width: 33%;">
    <br />
    <asp:Label ID="InvArg_lbl" runat="server" style="position:absolute; left:48%"></asp:Label>
    <br />
    <asp:Button ID="Load_btn" runat="server" OnClick="Load_btn_Click" Text="Load" ToolTip="Get current cron string to settings" Width="15%" CssClass="loadButtonStyle" />
    <asp:Button ID="Save_btn" runat="server" OnClick="Save_btn_Click" Text="Save" ToolTip="Set current settings to cron string" Width="15%" CssClass="saveButtonStyle" />
</div>
<asp:TextBox ID="CronString_tb" runat="server" style="margin-left: 33%" Width="33%"></asp:TextBox>




    </form>
</body>
</html>
