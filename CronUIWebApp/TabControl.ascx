﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabControl.ascx.cs" Inherits="CronUIWebApp.TabControl" %>


<link href="css/TimePicker.css" rel="stylesheet" type="text/css" />
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" media="all" rel="stylesheet" />
<link href="css/StyleSheet.css" rel="stylesheet" type="text/css" />


<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="http://code.jquery.com/jquery-1.9.1.js" />
        <asp:ScriptReference Path="http://code.jquery.com/ui/1.10.3/jquery-ui.js" />
        <asp:ScriptReference Path="~/js/TimePicker.js" />
        <asp:ScriptReference Path="~/js/Sliders.js" />
    </Scripts>
</ajaxToolkit:ToolkitScriptManager>
<asp:Panel ID="ShedulePanel" runat="server" GroupingText="Shedule" Height="312px" style="margin-left: 33%" Width="33%">
    <asp:RadioButton ID="Weekly_rb" runat="server" AutoPostBack="True" GroupName="RBGrp" Text="Weekly" OnCheckedChanged="Weekly_rb_CheckedChanged" />
            <br />
            <asp:Label ID="Label4" runat="server" Text="Every" CssClass="labelStyle" ></asp:Label>
    <asp:DropDownList ID="DaySelect_ddl" runat="server" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="DaySelect_ddl_SelectedIndexChanged" CssClass="textboxStyle" >
        <asp:ListItem>Sunday</asp:ListItem>
        <asp:ListItem>Monday</asp:ListItem>
        <asp:ListItem>Tuesday</asp:ListItem>
        <asp:ListItem>Wednesday</asp:ListItem>
        <asp:ListItem>Thursday</asp:ListItem>
        <asp:ListItem>Friday</asp:ListItem>
        <asp:ListItem>Saturday</asp:ListItem>
    </asp:DropDownList>
            <br />
            <br />
    <asp:Label ID="Label5" runat="server" Text="At" CssClass="labelStyle"></asp:Label>
    <asp:TextBox ID="SetTimeAtWeekly_tb" runat="server" Enabled="False" OnTextChanged="SetTimeAtWeekly_tb_TextChanged"  Height="16px" CssClass="textboxStyle"></asp:TextBox>
    <br />
    <br />
    <br />
    <asp:RadioButton ID="Daily_rb" runat="server" AutoPostBack="True" GroupName="RBGrp" OnCheckedChanged="Daily_rb_CheckedChanged" Text="Daily" />
    <asp:Label ID="Label6" runat="server" Text="Each" CssClass="labelStyle"></asp:Label>
    <asp:TextBox ID="eachNMin_tb" runat="server" Enabled="False" Height="16px" Width="85px" OnTextChanged="eachNMin_tb_TextChanged" CssClass="textboxStyle"></asp:TextBox>
            <asp:Label ID="Label9" runat="server" Text="minute" CssClass="lastLabelStyle"></asp:Label>
            <br />
            <br />
            <br />
            <asp:RadioButton ID="Monthly_rb" runat="server" GroupName="RBGrp" Text="Monthly" AutoPostBack="True" OnCheckedChanged="Monthly_rb_CheckedChanged" />
            <asp:Label ID="Label10" runat="server" Text="Each" CssClass="labelStyle"></asp:Label>
            <asp:TextBox ID="SetMontly_tb" runat="server" Enabled="False" AutoPostBack="True" Height="16px" OnTextChanged="SetMontly_tb_TextChanged" CssClass="textboxStyle"></asp:TextBox>
            <asp:Label ID="Label11" runat="server" Text="days" CssClass="lastLabelStyle"></asp:Label>
            <br />
            <br />
            <br />
            <asp:RadioButton ID="Custom_rb" runat="server" GroupName="RBGrp" Text="Custom" AutoPostBack="True" OnCheckedChanged="Custom_rb_CheckedChanged" />
            <br />
        </asp:Panel>
<asp:Panel ID="Panel1" runat="server" style="margin-left: 33%" Width="33%">
    <br />
    <ajaxToolkit:TabContainer ID="TabContainer" runat="server" ActiveTabIndex="4" Height="236px" Width="100%">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1"><HeaderTemplate>Minute</HeaderTemplate><ContentTemplate><asp:RadioButton ID="atMin_rb" runat="server" AutoPostBack="True" GroupName="minute" OnCheckedChanged="atMin_rb_CheckedChanged" Text="At:" /><asp:TextBox ID="minute_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="minute_tb_TextChanged" CssClass="customTextboxStyle" ></asp:TextBox><br /><br /><asp:RadioButton ID="rangeMin_rb" runat="server" AutoPostBack="True" GroupName="minute" OnCheckedChanged="rangeMin_rb_CheckedChanged" Text="From:" /><asp:TextBox ID="fromMin_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="fromMin_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><asp:Label ID="Label8" runat="server" Text="To" CssClass="midCustomLabelStyle"></asp:Label><asp:TextBox ID="toMin_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="toMin_tb_TextChanged" CssClass="customLastTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyNMin_rb" runat="server" AutoPostBack="True" GroupName="minute" OnCheckedChanged="everyNMin_rb_CheckedChanged" Text="Every:" />
            <asp:TextBox ID="everyNMin_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="everyNMin_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyMin_rb" runat="server" AutoPostBack="True" GroupName="minute" OnCheckedChanged="everyMin_rb_CheckedChanged" Text="Every Minute" /></ContentTemplate></ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2"><HeaderTemplate>Hour</HeaderTemplate><ContentTemplate><asp:RadioButton ID="atHr_rb" runat="server" AutoPostBack="True" GroupName="hour" OnCheckedChanged="atHr_rb_CheckedChanged" Text="At:" /><asp:TextBox ID="hour_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="hour_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="rangeHr_rb" runat="server" AutoPostBack="True" GroupName="hour" OnCheckedChanged="rangeHr_rb_CheckedChanged" Text="From:" /><asp:TextBox ID="fromHr_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="fromHr_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><asp:Label ID="Label7" runat="server" Text="To" CssClass="midCustomLabelStyle"></asp:Label><asp:TextBox ID="toHr_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="toHr_tb_TextChanged" CssClass="customLastTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyNHr_rb" runat="server" AutoPostBack="True" GroupName="hour" OnCheckedChanged="everyNHr_rb_CheckedChanged" Text="Every:" />
            <asp:TextBox ID="everyNHr_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="everyNHr_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyHr_rb" runat="server" AutoPostBack="True" GroupName="hour" OnCheckedChanged="everyHr_rb_CheckedChanged" Text="Every Hour" /></ContentTemplate></ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3"><HeaderTemplate>Day</HeaderTemplate><ContentTemplate><asp:RadioButton ID="atDay_rb" runat="server" AutoPostBack="True" GroupName="day" OnCheckedChanged="atDay_rb_CheckedChanged" Text="At:" /><asp:TextBox ID="day_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="day_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="rangeDay_rb" runat="server" GroupName="day" Text="From:" AutoPostBack="True" OnCheckedChanged="rangeDay_rb_CheckedChanged" /><asp:TextBox ID="fromDay_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="fromDay_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><asp:Label ID="Label1" runat="server" Text="To" CssClass="midCustomLabelStyle"></asp:Label><asp:TextBox ID="toDay_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="toDay_tb_TextChanged" CssClass="customLastTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyNDay_rb" runat="server" AutoPostBack="True" GroupName="day" OnCheckedChanged="everyNDay_rb_CheckedChanged" Text="Every:" />
            <asp:TextBox ID="everyNDay_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="everyNDay_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyDay_rb" runat="server" AutoPostBack="True" GroupName="day" OnCheckedChanged="everyDay_rb_CheckedChanged" Text="Every Day" /></ContentTemplate></ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" HeaderText="TabPanel4"><HeaderTemplate>Month</HeaderTemplate><ContentTemplate><asp:RadioButton ID="atMon_rb" runat="server" AutoPostBack="True" GroupName="month" OnCheckedChanged="atMon_rb_CheckedChanged" Text="At:" /><asp:TextBox ID="month_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="month_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="rangeMon_rb" runat="server" GroupName="month" Text="From:" AutoPostBack="True" OnCheckedChanged="rangeMon_rb_CheckedChanged" /><asp:TextBox ID="fromMon_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="fromMon_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><asp:Label ID="Label2" runat="server" Text="To" CssClass="midCustomLabelStyle"></asp:Label><asp:TextBox ID="toMon_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="toMon_tb_TextChanged" CssClass="customLastTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyNMon_rb" runat="server" AutoPostBack="True" GroupName="month" OnCheckedChanged="everyNMon_rb_CheckedChanged" Text="Every:" />
            <asp:TextBox ID="everyNMonth_tb" runat="server" Enabled="False" Height="16px" OnTextChanged="everyNMonth_tb_TextChanged" CssClass="customTextboxStyle"></asp:TextBox><br /><br /><asp:RadioButton ID="everyMon_rb" runat="server" AutoPostBack="True" GroupName="month" OnCheckedChanged="everyMon_rb_CheckedChanged" Text="Every Month" /></ContentTemplate></ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel5" runat="server" HeaderText="TabPanel5"><HeaderTemplate>Day of Week</HeaderTemplate><ContentTemplate><asp:RadioButton ID="atDoW_rb" runat="server" AutoPostBack="True" GroupName="dow" OnCheckedChanged="atDoW_rb_CheckedChanged" Text="Day of Week" />
            <asp:DropDownList ID="atDoW_ddl" runat="server" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="atDoW_ddl_SelectedIndexChanged" CssClass="customDdlStyle">
                <asp:ListItem>Sunday</asp:ListItem>
                <asp:ListItem>Monday</asp:ListItem>
                <asp:ListItem>Tuesday</asp:ListItem>
                <asp:ListItem>Wednesday</asp:ListItem>
                <asp:ListItem>Thursday</asp:ListItem>
                <asp:ListItem>Friday</asp:ListItem>
                <asp:ListItem>Saturday</asp:ListItem>
            </asp:DropDownList>
            <br /><br /><asp:RadioButton ID="rangeDoW_rb" runat="server" AutoPostBack="True" GroupName="dow" OnCheckedChanged="rangeDoW_rb_CheckedChanged1" Text="From: " />&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="fromDoW_ddl" runat="server" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="fromDoW_ddl_SelectedIndexChanged" CssClass="customDdlStyle">
                <asp:ListItem>Sunday</asp:ListItem>
                <asp:ListItem>Monday</asp:ListItem>
                <asp:ListItem>Tuesday</asp:ListItem>
                <asp:ListItem>Wednesday</asp:ListItem>
                <asp:ListItem>Thursday</asp:ListItem>
                <asp:ListItem>Friday</asp:ListItem>
                <asp:ListItem>Saturday</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="Label3" runat="server" Text="To" CssClass="midCustomDdlLabelStyle"></asp:Label>
            <asp:DropDownList ID="toDoW_ddl" runat="server" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="toDoW_ddl_SelectedIndexChanged" CssClass="customLastDdlStyle">
                <asp:ListItem>Sunday</asp:ListItem>
                <asp:ListItem>Monday</asp:ListItem>
                <asp:ListItem>Tuesday</asp:ListItem>
                <asp:ListItem>Wednesday</asp:ListItem>
                <asp:ListItem>Thursday</asp:ListItem>
                <asp:ListItem>Friday</asp:ListItem>
                <asp:ListItem>Saturday</asp:ListItem>
            </asp:DropDownList>
            <br /><br /><asp:RadioButton ID="workdays_rb" runat="server" AutoPostBack="True" GroupName="dow" OnCheckedChanged="workdays_rb_CheckedChanged" Text="Monday-Friday" /><br /><br /><asp:RadioButton ID="everyDoW_rb" runat="server" AutoPostBack="True" GroupName="dow" OnCheckedChanged="everyDoW_rb_CheckedChanged" Text="Every Day of Week" /></ContentTemplate></ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Panel>






<ajaxToolkit:CollapsiblePanelExtender ID="Panel1_CollapsiblePanelExtender" runat="server" 
    Enabled="True" 
    TargetControlID="Panel1"
    Collapsed="true"
    CollapsedText="Custom"
    ExpandedText="Custom"
    ExpandDirection="Vertical"
    ExpandControlID="Custom_rb">
</ajaxToolkit:CollapsiblePanelExtender>

<script>
    function LoadTimePickers() {
        $('#<%=SetTimeAtWeekly_tb.ClientID %>').timepicker();
    }
    function LoadMinutePickers() {
        $('#<%=eachNMin_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            stepMinute: 5
        });
        $('#<%=minute_tb.ClientID %>').timepicker({
            timeFormat: 'mm'
        });
        $('#<%=fromMin_tb.ClientID %>').timepicker({
            timeFormat: 'mm'
        });
        $('#<%=toMin_tb.ClientID %>').timepicker({
            timeFormat: 'mm'
        });
        $('#<%=everyNMin_tb.ClientID %>').timepicker({
            timeFormat: 'mm'
        });
    }
    function LoadHourPickers() {
        $('#<%=hour_tb.ClientID %>').timepicker({
            timeFormat: 'HH'
        });
        $('#<%=fromHr_tb.ClientID %>').timepicker({
            timeFormat: 'HH'
        });
        $('#<%=toHr_tb.ClientID %>').timepicker({
            timeFormat: 'HH'
        });
        $('#<%=everyNHr_tb.ClientID %>').timepicker({
            timeFormat: 'HH'
        });
    }
    function LoadDayPickers() {
        $('#<%=day_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Day',
            minuteMin: 1,
            minuteMax: 31,
            currentText: ''
        });
        $('#<%=fromDay_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Day',
            minuteMin: 1,
            minuteMax: 31,
            currentText: ''
        });
        $('#<%=toDay_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Day',
            minuteMin: 1,
            minuteMax: 31,
            currentText: ''
        });
        $('#<%=everyNDay_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Day',
            minuteMin: 1,
            minuteMax: 31,
            currentText: ''
        });
    }
    function LoadMonthPickers() {
        $('#<%=month_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Month',
            minuteMin: 1,
            minuteMax: 12,
            currentText: ''
        });
        $('#<%=fromMon_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Month',
            minuteMin: 1,
            minuteMax: 12,
            currentText: ''
        });
        $('#<%=toMon_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Month',
            minuteMin: 1,
            minuteMax: 12,
            currentText: ''
        });
        $('#<%=everyNMonth_tb.ClientID %>').timepicker({
            timeFormat: 'mm',
            minuteText: 'Month',
            minuteMin: 1,
            minuteMax: 12,
            currentText: ''
        });
    }
    $(document).ready(function () {
        LoadTimePickers();
        LoadMinutePickers();
        LoadHourPickers();
        LoadDayPickers();
        LoadMonthPickers();
    });
</script>







